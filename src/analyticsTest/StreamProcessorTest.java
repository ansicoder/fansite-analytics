package src.analyticsTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import analytics.StreamProcessor;

public class StreamProcessorTest {
	
	String[] input = {
			"199.72.81.55 - - [01/Jul/1995:00:00:01 -0400] \"POST /login HTTP/1.0\" 401 1420", 
			"unicomp6.unicomp.net - - [01/Jul/1995:00:00:06 -0400] \"GET /shuttle/countdown/ HTTP/1.0\" 200 3985",
			"199.72.81.55 - - [01/Jul/1995:00:00:09 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"burger.letters.com - - [01/Jul/1995:00:00:11 -0400] \"GET /shuttle/countdown/liftoff.html HTTP/1.0\" 304 0",
			"199.72.81.55 - - [01/Jul/1995:00:00:12 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"199.72.81.55 - - [01/Jul/1995:00:00:13 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"199.72.81.55 - - [01/Jul/1995:00:00:14 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"burger.letters.com - - [01/Jul/1995:00:00:14 -0400] \"GET /shuttle/countdown/ HTTP/1.0\" 200 3985",
			"burger.letters.com - - [01/Jul/1995:00:00:15 -0400] \"GET /shuttle/countdown/liftoff.html HTTP/1.0\" 304 0",
			"199.72.81.55 - - [01/Jul/1995:00:00:15 -0400] \"POST /login HTTP/1.0\" 401 1420" };
	
	String[] out1 = {"199.72.81.55,6", "burger.letters.com,3", "unicomp6.unicomp.net,1"};
	String[] out2 = {"/login", "/shuttle/countdown/", "/shuttle/countdown/liftoff.html"};
	String[] out3 = {
			"01/Jul/1995:00:00:01 -0400,10",
			"01/Jul/1995:00:00:02 -0400,9",
			"01/Jul/1995:00:00:03 -0400,9",
			"01/Jul/1995:00:00:04 -0400,9",
			"01/Jul/1995:00:00:05 -0400,9",
			"01/Jul/1995:00:00:06 -0400,9",
			"01/Jul/1995:00:00:07 -0400,8",
			"01/Jul/1995:00:00:08 -0400,8",
			"01/Jul/1995:00:00:09 -0400,8",
			"01/Jul/1995:00:00:10 -0400,7"};
	String[] out4 = {
			"199.72.81.55 - - [01/Jul/1995:00:00:13 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"199.72.81.55 - - [01/Jul/1995:00:00:14 -0400] \"POST /login HTTP/1.0\" 401 1420",
			"199.72.81.55 - - [01/Jul/1995:00:00:15 -0400] \"POST /login HTTP/1.0\" 401 1420"};
	
	@Test
	/**
	 * Test StreamProcessor functionality end-to-end
	 */
	public void testStreamProcessorEndToEnd()
	{
		StreamProcessor sp = new StreamProcessor();
		for (String line : input)
		{
			sp.processLine(line);
		}
		List<String> res1 = sp.topActivityByHost();
		List<String> res2 = sp.topBandwidthByResource();
		List<String> res3 = sp.topBusyPeriods();
		List<String> res4 = sp.invalidLoginAttempts();
		
		for (int i = 0; i < out1.length; i++)
		{
			assertEquals(res1.get(i), out1[i]);
			assertEquals(res2.get(i), out2[i]);
			assertEquals(res3.get(i), out3[i]);
			assertEquals(res4.get(i), out4[i]);
		}
	}
}