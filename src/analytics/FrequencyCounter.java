package src.analytics;

import java.util.Date;

/**
 * 
 * @author Mansi
 * Helper class to track counts within a start time and end time
 */
public class FrequencyCounter implements Comparable<FrequencyCounter> {
	private Date dateFrom;
	private Date dateTo;
	private long counter;
	
	/**
	 * Constructor 
	 * @param date : start time this object tracks after
	 * @param window : period window this object tracks within starting from date
	 */
	public FrequencyCounter(Date date, int window)
	{
		this.dateFrom = date;
		this.dateTo = new Date(date.getTime() + window);
		this.counter = 1;
	}
	
	public long getCount() { return this.counter; }	
	public void setCount(long count) { this.counter = count; }	
	public Date getFromDate() { return dateFrom; }	
	public Date getToDate() { return dateTo; }
	
	@Override
	/**
	 * Compares counts, if counts are equal then compares dates where earlier date is larger
	 */
	public int compareTo(FrequencyCounter that)
	{		
		int res = (int) (this.getCount() - that.getCount());
		if (res == 0)
		{
			res = that.getFromDate().compareTo(this.getFromDate());
		}
		
		return res;
	}
	
}