package src.analytics;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Mansi
 * Entry point for the application, contains main()
 */
public class Driver {

	public static void main(String[] args) {
		
		if (args.length < 5)
		{
			System.out.println(
				"Usage: ./analytics/Driver.java " +
				"./log_input/log.txt ./log_output/hosts.txt " +
				"./log_output/hours.txt ./log_output/resources.txt " +
				"./log_output/blocked.txt [./log_output/activity.txt]");
			return;
		}
		
		String inLog       = args[0];
		String outHost	   = args[1];
		String outResource = args[3];
		String outPeriod   = args[2];
		String outBlocked  = args[4];
		String outActivity = null;
		if (args.length == 6)
			outActivity = args[5];
		
		StreamProcessor sp = new StreamProcessor();
		List<String> out;		
		Charset utf8 = StandardCharsets.UTF_8;		
		long startTime = System.currentTimeMillis();
		
		try
		{
			/* process the input log */
			sp.processLog(inLog, true);
		
			/* Feature 1: get top 10 active host/ip addresses */
			out = sp.topActivityByHost();
		    Files.write(Paths.get(outHost), out, utf8);
			
			/* Feature 2: get top 10 bandwidth heavy resources */
		    out = sp.topBandwidthByResource();
		    Files.write(Paths.get(outResource), out, utf8);
			
			/* Feature 3: get top 10 hour-long busy periods */
		    out = sp.topBusyPeriods();
		    Files.write(Paths.get(outPeriod), out, utf8);
			
			/* Feature 4: get blockable IPs' list */
		    out = sp.invalidLoginAttempts();
		    Files.write(Paths.get(outBlocked), out, utf8);
			
		    if (outActivity != null)
		    {
				/* Bonus Feature: get activity histogram over each day */
		    	out = sp.activityHistogramByDay();
		    	Files.write(Paths.get(outActivity), out, utf8);
		    }
		}
		catch (IOException e)
		{
		    e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		double totalTime = (endTime - startTime)/1000.0;
		System.out.println("Time Taken (s): " + totalTime);
	}

}