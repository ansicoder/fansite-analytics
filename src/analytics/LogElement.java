package src.analytics;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Mansi
 * Represents an object view of a line of a log. Only relevant fields are tracked
 */
public class LogElement {
	private final String host;
	private final Date date;
	private final String resource;
	private final Integer code;
	private final Long bytes;
	
	public LogElement(String host, Date date, String resource, Integer code, Long bytes)
	{
		this.host = host;
		this.date = date;
		this.resource = resource;
		this.code = code;
		this.bytes = bytes;
	}

	public String getHost() { return host; }
	public Date getDate() {	return date; }
	public String getResource() { return resource; }
	public Integer getCode() { return code; }
	public Long getBytes() { return bytes; }
	
	/**
	 * get next index of next delim character starting at index i
	 */
	private static int getIndexOf(char[] cline, char delim, int i)
	{
		while (i < cline.length && cline[i] != delim) i++;
		return i;
	}
	
	/**
	 * get next index after skipping all delim characters starting at index i
	 */
	private static int skipAll(char[] cline, char delim, int i)
	{
		while (i < cline.length && cline[i] == delim) i++;
		return i;
	}
	
	/**
	 * get last index of given delim character starting from index i at the end
	 */
	private static int getIndexOfLast(char[] cline, char delim, int i)
	{
		while (i >= 0 && cline[i] != delim) i--;
		return i;
	}
	
	/**
	 * ensure index ranges are valid
	 */
	private static boolean rangeFine(int start, int end)
	{
		return (start >= 0) && (end >= 0) && (start < end);
	}
	
	/**
	 * Parses a log line into LogElement attributes. Able to handle unexpected spaces
	 * and some fields. However, the general expected format of the line should be adhered to
	 * otherwise returns null or in extreme cases may throw exception and output a trace
	 * @param line : a line of a log to be parsed
	 * @return LogElement object corresponding to the line
	 */
	public static LogElement ElementParser(String line)
	{
		/* Assumes format similar to:
		 * 219.256.93.25 - - [26/Jul/1995:08:11:31 -0400] "GET /facts/acron2.txt HTTP/1.0" 200 47101
		 */
		String host = null, datestr = null, resource = null;
		Date date = null;
		Integer code = null;
		Long bytes = null;
		char[] cline;
		
		try
		{
			cline = line.toCharArray();
			int start = 0, end;
			/* skip until a space is encountered */
			end = getIndexOf(cline, ' ', start);
			if (!rangeFine(start, end))
				return null;
			/* anything before space is host */
	 		host = line.substring(start, end);
	 		
	 		/* find range of [] */
	 		start = getIndexOf(cline, '[', end) + 1;
	 		end = getIndexOf(cline, ']', start);
	 		if (!rangeFine(start, end))
				return null;
	 		/* get date string */
	 		datestr = line.substring(start, end);
	 		SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT);
	 		/* parse date string into Date object */
			date = sdf.parse(datestr);
	 		
			/* skip to / to get start of resource */
	 		start = getIndexOf(cline, '/', end);
	 		int endQuote = getIndexOfLast(cline, '"', cline.length-1);
	 		end = getIndexOf(cline, ' ', start); 	
	 		/* check if resource is followed by a quote or additional information, only extract resource url */
	 		if (end > endQuote)
	 			end = endQuote; 	
	 		if (!rangeFine(start, end))
				return null;
	 		resource = line.substring(start, end);
	 		
	 		end = endQuote+1;
	 		/* skip spaces until first number found */
	 		start = skipAll(cline, ' ', end);
	 		end = getIndexOf(cline, ' ', start);
	 		if (!rangeFine(start, end))
				return null;
	 		/* get status code */
	 		code = Integer.parseInt(line.substring(start, end));
	 		
	 		bytes = (long) 0; 
	 		/* skip spaces until second number or '-'found */
	 		start = skipAll(cline, ' ', end);
	 		if (cline[start] != '-')
	 		{
	 			if (!rangeFine(start, cline.length))
					return null;
	 			/* get bytes */
	 			bytes = Long.parseLong(line.substring(start, cline.length));
	 		}
		}
		catch (Exception e)
		{
			System.out.println(line);
			e.printStackTrace();
		}
		
		return new LogElement(host, date, resource, code, bytes);
	}
	
	/**
	 * A simple parser that returns null if the format deviates from the expected format
	 * @param line : a line of a log to be parsed
	 * @return LogElement object corresponding to the line
	 */
	public static LogElement ElementParserSimple(String line)
	{
		/* Assumes format same as:
		 * 219.256.93.25 - - [26/Jul/1995:08:11:31 -0400] "GET /facts/acron2.txt HTTP/1.0" 200 47101
		 * 0: IP
		 * 1: -
		 * 2: -
		 * 3: [Date
		 * 4: Zone]
		 * 5: Request type
		 * 6: URL
		 * 7: HTTP/1.0...
		 * 8: Status code
		 * 9: Bytes
		 */
		String[] field = line.split(" ");
		if (field.length != Config.N_FIELDS)
			return null;
		
		Date date = null;
		Integer code = null;
		Long bytes = (long) 0;
		SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT_WITH_BRACKET);
		
		try
		{			
			date = sdf.parse(field[3] + " " + field[4]);
			code = Integer.valueOf(field[8]);
			if (!field[9].equals("-"))
				Long.valueOf(field[9]);
		}
		catch (Exception e)
		{
			System.out.println(line);
			e.printStackTrace();
		}
		
		return new LogElement(field[0], date, field[6], code, bytes);
	}
}