package src.analytics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.TimeZone;

/**
 * 
 * @author Mansi
 * Encapsulates application's functionality. Takes in a log or a line and analyzes
 * based on functions available 
 */
public class StreamProcessor {
	
	private final int TOP_COUNT = Config.TOP_COUNT;
	private HashMap<String, Long> mapHostCount;
	private HashMap<String, Long> mapResourceCount;	
	private PriorityQueue<Entry<String, Long>> minHeapTopN;
	private LinkedList<FrequencyCounter> listBusyPeriod;
	private PriorityQueue<FrequencyCounter> minHeapTopNBusyPeriods;
	private HashMap<String, LinkedList<FrequencyCounter>> mapLoginAttempt;
	private ArrayList<String> listInvalidLogin;
	private ArrayList<FrequencyCounter> listActivityHistogram;
	
	/**
	 * 
	 * @author Mansi
	 * Comparator to compare a Map.Entry<>, first on integer value, then on string key (lexicographic) 
	 */
	private class CountCompare implements Comparator<Entry<String, Long>>
	{
		@Override
		public int compare(Entry<String, Long> e1, Entry<String, Long> e2)
		{
			int res = (int)(e1.getValue() - e2.getValue());
			
			if (res == 0)			
				res = e2.getKey().compareTo(e1.getKey());
			
			return res;
		}		
	}
	
	/**
	 * Initialize internal data structures
	 */
	private void initAttributes()
	{
		mapHostCount = new HashMap<>();
		mapResourceCount = new HashMap<>();
		minHeapTopN = new PriorityQueue<Entry<String, Long>>(TOP_COUNT, new CountCompare());
		listBusyPeriod = new LinkedList<>();
		minHeapTopNBusyPeriods = new PriorityQueue<>(TOP_COUNT);
		mapLoginAttempt = new HashMap<>();
		listInvalidLogin = new ArrayList<>();
		listActivityHistogram = new ArrayList<>();
	}	
	
	public StreamProcessor()
	{
		initAttributes();
	}
	
	/**
	 * Public interface to process a log line and maintain data structures based
	 * on desired functionality
	 * @param line : a line of a log to be parsed
	 */
	public void processLine(String line)
	{
		/* parse given line */
		LogElement e = LogElement.ElementParser(line);
		if (e != null)
		{
			/* check if line represents a frequent login activity */
			addFrequentLogin(line, e.getHost(), e.getDate(), e.getCode());
			/* add given host to corresponding map and maintain cumulative count */
			addHostToMap(e.getHost());
			/* add given resource to corresponding map and maintain cumulative bytes */
			addResourceToMap(e.getResource(), e.getBytes());
			/* add given request to corresponding period of activity */
			addBusyPeriod(e.getDate());
			/* add activity based on its date and period being tracked, maintains a histogram */
			addActivityToPeriod(e.getDate(), Config.DEFAULT_PERIOD);
		}
	}
	
	/**
	 * Public interface to process a log file and maintain data structures based
	 * on desired functionality
	 * @param fileName : name of the log file
	 * @param dropCurrent : start afresh if true
	 */
	public void processLog(String fileName, boolean dropCurrent)
	{
		if (dropCurrent)
			initAttributes();
		
		BufferedReader reader = null;		 
		try
		{			 
			reader = new BufferedReader(new FileReader(fileName));
			String line;		
			
			/* read a line at a time, from given file */
			while ((line = reader.readLine()) != null)
			{
				processLine(line);
			}
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		finally
		{
			try
			{
				if (reader != null)
					reader.close();
			}
			catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
	}	
	
	/**
	 * returns a list of top active host/ip addresses
	 * @return
	 */
	public List<String> topActivityByHost()
	{
		LinkedList<String> result = new LinkedList<String>();
		
		initTop(mapHostCount);
		extractTop(result, true);
		 
		return result;
	}
	
	/**
	 * returns a list of top bandwidth heavy resources
	 * @return
	 */
	public List<String> topBandwidthByResource()
	{
		LinkedList<String> result = new LinkedList<String>();
		
		initTop(mapResourceCount);
		extractTop(result, false);
		 
		return result;
	}
	
	/**
	 * returns a list of top busy periods
	 * @return
	 */
	public List<String> topBusyPeriods()
	{
		LinkedList<String> result = new LinkedList<String>();
		
		fillTopNBusyPeriods(listBusyPeriod);
		extractTopNBusyPeriods(result, true);	 
		return result;
	}
	
	/**
	 * returns list of blockable login attempts
	 * @return
	 */
	public List<String> invalidLoginAttempts()
	{
		return listInvalidLogin;		
	}
	
	/**
	 * returns a histogram of activity by day
	 * @return
	 */
	public List<String> activityHistogramByDay()
	{
		List<String> result = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		for (FrequencyCounter fc : listActivityHistogram)
			result.add(sdf.format(fc.getFromDate()) + "," + fc.getCount());
		return result;
	}
	
	/**
	 * Adds given activity to corresponding period
	 * @param date : time at which activity happened
	 * @param period : desired period length
	 */
	private void addActivityToPeriod(Date date, int period)
	{
		FrequencyCounter fc;
		
		if (listActivityHistogram.size() > 0)
		{
			fc = listActivityHistogram.get(listActivityHistogram.size()-1);
			if (!date.after(fc.getToDate()))
			{
				fc.setCount(fc.getCount()+1);
				return;
			}
		}
		
		Date start = new Date(date.getTime() - (date.getTime() % period));
		fc = new FrequencyCounter(start, period);
		listActivityHistogram.add(fc);		
	}
	
	/**
	 * Detect frequent logins from same host based on defined criteria
	 * @param line : full request string
	 * @param host : source host 
	 * @param date : time of activity
	 * @param code : HTTP status code to detect failure or success
	 */
	private void addFrequentLogin(String line, String host, Date date, Integer code)
	{
		LinkedList<FrequencyCounter> list = null;
		FrequencyCounter newNode = null;
		
		if (mapLoginAttempt.containsKey(host))
			list = mapLoginAttempt.get(host);
		
		if (list == null)
		{
			list = new LinkedList<FrequencyCounter>();
			mapLoginAttempt.put(host, list);
		}
		
		if (list.size() == 0)
		{
			/* if no entry found, then it means this is the first attempt OR last attempt was successful
			 * and was not within penalty window. Put an entry, only if current attempt is a failed login
			 * and start a new invalid login window */
			if (code == Config.HTTP_FAILED_LOGIN_CODE)
			{			
				newNode = new FrequencyCounter(date, Config.INVALID_LOGIN_WINDOW_IN_MS);
				list.addLast(newNode);
			}
			return;
		}
		
		/* if we haven't seen enough invalid login attempts in the invalid window */
		if (list.size() < Config.MAX_INVALID_LOGIN)
		{	
			int count = list.size();
			
			if (code != Config.HTTP_FAILED_LOGIN_CODE)
			{
				/* if current login is a valid login, then clear all entries (reset) */
				list.clear();
			}
			else if (date.after(list.getFirst().getToDate()))
			{
				/* if current login attempt is beyond the invalid window, then reset invalid window
				 * to start from next list element time */
				while (list.size() > 0 && date.after(list.getFirst().getToDate()))
				{
					list.removeFirst();					
				}
				newNode = new FrequencyCounter(date, Config.INVALID_LOGIN_WINDOW_IN_MS);
				list.addLast(newNode);
			}			
			else if (count == (Config.MAX_INVALID_LOGIN-1))
			{
				/* if this is the last invalid login allowed within this window,
				 * begin penalty window starting from current time */
				newNode = new FrequencyCounter(date, Config.PENALTY_LOGIN_WINDOW_IN_MS);
				list.addLast(newNode);
			}
			else
			{				
				/* put this invalid attempt in the list */
				newNode = new FrequencyCounter(date, Config.INVALID_LOGIN_WINDOW_IN_MS);
				list.addLast(newNode);
			}
		}
		else /* maximum invalid login attempts done */
		{
			if (!date.after(list.getLast().getToDate()))
			{
				/* add to invalid login list if still in penalty window */
				listInvalidLogin.add(line);
			}
			else if (code == Config.HTTP_FAILED_LOGIN_CODE)
			{
				/* if outside penalty window, but failed login attempt, begin a new
				 * invalid login window */
				list.clear();
				newNode = new FrequencyCounter(date, Config.INVALID_LOGIN_WINDOW_IN_MS);
				list.add(newNode);
			}
			else
			{
				/* if outside penalty window, and successful login attempt, stop tracking
				 * invalid logins */
				list.clear();
			}
		}
	}

	/**
	 * Given a buffer of time-sorted activity periods, puts them in
	 * a minheap to find top busy periods 
	 * @param buffer
	 */
	private void fillTopNBusyPeriods(LinkedList<FrequencyCounter> buffer) {
		Date interDate;
		FrequencyCounter fc;
		boolean firstElem = true;
		long count = 0;
		
		while (buffer.size() > 0)
		{
			/* get the start of a period */
			fc = buffer.removeFirst();
			if (firstElem)
			{
				/* count is only affected by a request that was actually registered in the log.
				 * For auto-generated periods consider count of the (original request count - 1) */
				count = fc.getCount();
				firstElem = false;
			}
			
			/* is minheap full? */
			if (minHeapTopNBusyPeriods.size() >= TOP_COUNT)
			{
				/* check if the minimum top period has a lower count than current, pop in that case */
				if (minHeapTopNBusyPeriods.peek().compareTo(fc) < 0)
					minHeapTopNBusyPeriods.remove();
				else
					continue;
			}
			/* add current to minheap */
			minHeapTopNBusyPeriods.add(fc);
			/* generate rest of the instances for starting from current instance not already in buffer,
			 * one by one, each one second apart */
			interDate = new Date(fc.getFromDate().getTime() + 1000);
			if (listBusyPeriod.size() > 0 && interDate.compareTo(listBusyPeriod.getFirst().getFromDate()) < 0)
			{					
				listBusyPeriod.addFirst(new FrequencyCounter(interDate, Config.LOAD_WINDOW_IN_MS));
				/* account for previous (original) instance of request in count */
				listBusyPeriod.getFirst().setCount(count-1);
			}
			else
				firstElem = true;
		}
	}

	/**
	 * Poll minheap to get top busy periods
	 * @param result : result string
	 * @param getCount : get count as well in output string
	 */
	private void extractTopNBusyPeriods(LinkedList<String> result, boolean getCount) {
		int len = minHeapTopNBusyPeriods.size();
		FrequencyCounter f;
		String res;
		SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone(Config.TIMEZONE));
		
		for (int i = len-1; i >= 0; i--)
		{
			f = minHeapTopNBusyPeriods.poll();
			res = sdf.format(f.getFromDate());
			if (getCount)
				res += "," + f.getCount();
			result.addFirst(res);
		}		
	}

	/**
	 * Adds a request at a particular time instance to the busy period buffer.
	 * Also, removes any prior instances that are now beyond the period in consideration.
	 * Also, maintains count (+1) for prior instances that still fall in the current period.
	 * @param date - time of current request
	 */
	private void addBusyPeriod(Date date) {
		FrequencyCounter fc;
		Date interDate;
		boolean firstElem = true;
		long count = 0;
		
		while (listBusyPeriod.size() > 0)
		{
			/* if this instance is beyond the first instance in this list, then
			 * this will contribute to its count. So, remove it. Also, while
			 * removing, auto-generate other instances one-second apart that are
			 * not in the busy list and maintain their counts.
			 * This is functionally similar to fillTopNBusyPeriods()
			 * TODO: extract common code in fillTopNBusyPeriods() and here into a
			 * separate block
			 */
			if (date.after(listBusyPeriod.getFirst().getToDate()))
			{
				fc = listBusyPeriod.removeFirst();
				if (firstElem)
				{					
					count = fc.getCount();
					firstElem = false;
				}
				if (minHeapTopNBusyPeriods.size() >= TOP_COUNT)
				{
					if (minHeapTopNBusyPeriods.peek().compareTo(fc) < 0)
						minHeapTopNBusyPeriods.remove();
					else
						continue;
				}
				minHeapTopNBusyPeriods.add(fc);
				interDate = new Date(fc.getFromDate().getTime() + 1000);
				if (listBusyPeriod.size() > 0 && interDate.compareTo(listBusyPeriod.getFirst().getFromDate()) < 0)
				{					
					listBusyPeriod.addFirst(new FrequencyCounter(interDate, Config.LOAD_WINDOW_IN_MS));
					listBusyPeriod.getFirst().setCount(count-1);
				}
				else
					firstElem = true;
			}
			else
			{
				/* maintain counts for instances that fall within current request instance */
				for (FrequencyCounter f : listBusyPeriod)
				{
					f.setCount(f.getCount()+1);
				}
				break;
			}				
		}
		
		if (listBusyPeriod.size() > 0 && listBusyPeriod.getLast().getFromDate().equals(date))
			return;
			
		listBusyPeriod.addLast(new FrequencyCounter(date, Config.LOAD_WINDOW_IN_MS));
	}

	/**
	 * Add host to corresponding map and maintain cumulative count
	 * @param ip
	 */
	private void addHostToMap(String ip) {
		Long oldVal = (long) 0;
		if (mapHostCount.containsKey(ip))
			oldVal = mapHostCount.get(ip);
		mapHostCount.put(ip, oldVal+1);
	}
	
	/**
	 * Add resource to corresponding map and maintain cumulative bytes used
	 * @param resource
	 * @param bytes
	 */
	private void addResourceToMap(String resource, Long bytes) {
		Long oldVal = (long) 0;
		if (mapResourceCount.containsKey(resource))
			oldVal = mapResourceCount.get(resource);
		mapResourceCount.put(resource, oldVal+bytes);
	}
	
	/**
	 * Extract top keys in minheap order (based on cumulative sum)
	 * @param result
	 * @param getCount
	 */
	private void extractTop(LinkedList<String> result, boolean getCount) {
		int len = minHeapTopN.size();
		Entry<String, Long> e;
		String res;
		
		for (int i = len-1; i >= 0; i--)
		{
			e = minHeapTopN.poll();
			res = e.getKey();
			if (getCount)
				res += "," + e.getValue();
			result.addFirst(res);
		}
	}

	/**
	 * Fill minheap by iterating over each map entry based on its cumulative sum
	 * @param map
	 */
	private void initTop(HashMap<String, Long> map) {
		for (Entry<String, Long> e : map.entrySet())
		{
			if (minHeapTopN.size() >= TOP_COUNT)
			{
				if (minHeapTopN.peek().getValue() < e.getValue())
					minHeapTopN.remove();
				else
					continue;
			}
			minHeapTopN.add(e);
		}		
	}
}