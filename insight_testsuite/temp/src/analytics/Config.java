package src.analytics;

/**
 * 
 * @author Mansi
 * Encloses configuration attributes that are common to various application functionality
 */
public class Config {
	static final int LOAD_WINDOW_IN_MS = 60000;
	static final int DEFAULT_PERIOD = (24*60*60*1000);
	static final int INVALID_LOGIN_WINDOW_IN_MS = 20000;
	static final int PENALTY_LOGIN_WINDOW_IN_MS = 300000;
	static final int MAX_INVALID_LOGIN = 3;
	static final int TOP_COUNT = 10;
	static final int HTTP_FAILED_LOGIN_CODE = 401;
	static final int N_FIELDS = 10;
	static final String DATE_FORMAT = "dd/MMM/yyyy:HH:mm:ss Z";
	static final String DATE_FORMAT_WITH_BRACKET = "'['dd/MMM/yyyy:HH:mm:ss Z']'";
	static final String TIMEZONE = "US/Eastern";
}