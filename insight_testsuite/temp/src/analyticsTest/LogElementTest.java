package src.analyticsTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import analytics.LogElement;

public class LogElementTest {
	
	final String DATE_FORMAT = "dd/MMM/yyyy:HH:mm:ss Z";
	
	@Test
	/**
	 * Test ElementParser parses normal line as expected
	 */
	public void testElementParserExpected()
	{
		String line = "219.256.93.25 - - [26/Jul/1995:08:11:31 -0400] \"GET /facts/acron2.txt HTTP/1.0\" 200 47101";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {
			date = sdf.parse("26/Jul/1995:08:11:31 -0400");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		LogElement elem = LogElement.ElementParser(line);
		assertEquals("219.256.93.25", elem.getHost());
		assertEquals(date, elem.getDate());
		assertEquals("/facts/acron2.txt", elem.getResource());
		assertEquals((Integer)200, elem.getCode());
		assertEquals((Long)(long)47101, elem.getBytes());		
	}
	
	@Test
	/**
	 * Test ElementParser gives up a null in face of garbage
	 */
	public void testElementParserNull()
	{
		String line = "219.256.93.25 - garbage - []";		
		LogElement elem = LogElement.ElementParser(line);
		assertEquals(null, elem);
	}
	
	@Test
	/**
	 * Test ElementParser handles unexpected spaces and field omissions
	 */
	public void testElementParserUnexpected()
	{
		String line = "219.256.93.25 -     -     [26/Jul/1995:08:11:31    -0400]    \"POST   /facts/acron2.txt \" 200 47101";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {
			date = sdf.parse("26/Jul/1995:08:11:31 -0400");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		LogElement elem = LogElement.ElementParser(line);
		assertEquals("219.256.93.25", elem.getHost());
		assertEquals(date, elem.getDate());
		assertEquals("/facts/acron2.txt", elem.getResource());
		assertEquals((Integer)200, elem.getCode());
		assertEquals((Long)(long)47101, elem.getBytes());
	}
}