package src.analyticsTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import analytics.FrequencyCounter;

public class FrequencyCounterTest {
	
	final String DATE_FORMAT = "dd/MMM/yyyy:HH:mm:ss Z";
	
	@Test
	/**
	 * Test FrequencyCounter object populated with correct date ranges
	 * when supplied with a window period
	 */
	public void testFrequencyCounterDateRange()
	{
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = sdf.parse("26/Jul/1995:08:11:31 -0400");
			toDate = sdf.parse("26/Jul/1995:09:11:31 -0400");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		FrequencyCounter fc = new FrequencyCounter(fromDate, 60*60*1000);
		assertEquals(toDate, fc.getToDate());
	}
	
	@Test
	/**
	 * Test comparison between FrequencyCounter objects is sorted on dates
	 * when counts are equal
	 */
	public void testFrequencyCounterCompare()
	{
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = sdf.parse("26/Jul/1995:08:11:31 -0400");
			date2 = sdf.parse("26/Jul/1995:09:11:31 -0400");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long count1 = 50;
		long count2 = 50;
		
		FrequencyCounter fc1 = new FrequencyCounter(date1, 60*60*1000);
		fc1.setCount(count1);
		FrequencyCounter fc2 = new FrequencyCounter(date2, 60*60*1000);
		fc2.setCount(count2);		
		int res = fc1.compareTo(fc2);
		/* assert that sorted by date in ascending order if counts are equal */
		assertEquals(1, res);
		res = fc2.compareTo(fc1);
		assertEquals(-1, res);
	}
}
